# frozen_string_literal: true

# SPDX-License-Identifier: AGPL-3.0-or-later

Gem::Specification.new do |s|
  s.name          = 'markdown-list-tools'
  s.version       = '0.1.0'
  s.summary       = 'Tools to convert Markdown lists to YAML and Freemind'
  s.description   = <<~DESC
    markdown-list-tools includes two utilities:
    * list-markdown2yaml: Convert Markdown lists to YAML
    * list-markdown2mm: Convert Markdown lists to Freemind/Freeplane/XMind *.mm
  DESC
  s.authors       = ['Fabio Pesari']
  s.homepage      = 'https://gitlab.com/fpesari/markdown-list-tools'
  s.files         = %w[.rubocop.yml AUTHORS LICENSE README.md bin/list-md2mm
                       bin/list-md2yaml lib/markdown_list_tools.rb
                       lib/markdown_list_tools/ast_parser.rb
                       lib/markdown_list_tools/converter/to_mind_map.rb
                       lib/markdown_list_tools/converter/to_yaml.rb
                       lib/markdown_list_tools/markdown_loader.rb
                       markdown-list-tools.gemspec test/fixtures/test.md]
  s.test_files    = Dir.glob 'test/*'
  s.require_paths = ['lib']
  s.executables   = %w[list-md2mm list-md2yaml]
  s.license       = 'AGPL-3.0+'
  s.add_runtime_dependency 'kramdown', '~> 2.0'
end
