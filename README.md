# markdown-list-tools - Convert Markdown lists to MindMap and YAML

`markdown-list-tools` includes two programs written in Ruby:
+ `list-md2mm` - Convert Markdown lists to the MindMap (*.mm)
format compatible with FreeMind, Freeplane and XMind (among others)
+ `list-md2yaml` - Convert Markdown lists to YAML

Markdown lists have to be saved as a Markdown file with this structure:

```md
# Map center. Required, otherwise an error will be thrown
* Entry 1
  * Entry 1.1
    * Entry 1.1.1
    * Entry 1.1.2
  * Entry 1.2
* Entry 2
  * Entry 2.1
* Entry 3
```

## list-md2mm

Usage:

```
list-md2mm FILE
```

Converts `FILE` to MindMap and outputs it to stdout. You can save it to a *.mm
file like this:

```
list-md2mm > file.mm
```

You can then modify the *.mm file in Freemind, Freeplan or XMind.

## list-md2yaml

Usage:

```
list-md2yaml FILE
```

Converts `FILE` to YAML and outputs it to stdout. You can save it to a *.yaml
file like this:

```
list-md2yaml > file.yaml
```

Generated YAML files will always follow this schema:

```yaml
title: Map center
list: []
```

## Installation

`markdown-list-tools` is packed as a RubyGem, so you can install it with:

```sh
gem build markdown-list-tools.gemspec
gem install markdown-list-tools-*.gem --user
```

(This is for the user-local install, for the system-wide install omit `--user`)

## License

md-list-tools is libre software released under the GNU AGPLv3+.
