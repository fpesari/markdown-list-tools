# frozen_string_literal: true

# SPDX-License-Identifier: AGPL-3.0-or-later

module MarkdownListTools
  # Traverse the AST to generate a tidy tree
  module ASTParser
    # Exception raised if the user-specified Markdown file has a wrong structure
    class SchemaError < RuntimeError
      def initialize(msg = 'Wrong schema for the Markdown list')
        super msg
      end
    end

    class << self
      def traverse(list)
        list.reduce([]) do |a, e|
          entry = e[:children].first[:children].first[:value].to_s.strip
          a << if e[:children][1]
                 { entry => traverse(e[:children][1][:children]) }
               else
                 entry
               end
          a
        end
      end

      def parse(tree)
        raise SchemaError unless tree.length == 2

        raise SchemaError unless tree.first[:type] == :header

        raise SchemaError unless %i[ul ol].any? tree[1][:type]

        title = tree.first[:children].first[:value]
        { 'title' => title, 'list' => traverse(tree[1][:children]) }
      end
    end
  end
end
