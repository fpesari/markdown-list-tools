# frozen_string_literal: true

# SPDX-License-Identifier: AGPL-3.0-or-later

require 'kramdown'

module MarkdownListTools
  # Load Markdown files into an AST
  module MarkdownLoader
    class << self
      def load_file(filename)
        content = File.read filename
        Kramdown::Document.new(content.strip.gsub(/\r?\n+/, "\n")).to_hash_ast
      end
    end
  end
end
