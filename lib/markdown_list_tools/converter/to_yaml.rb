# frozen_string_literal: true

# SPDX-License-Identifier: AGPL-3.0-or-later

require 'yaml'

module MarkdownListTools
  module Converter
    # YAML converter
    module ToYAML
      class << self
        def convert(tree)
          puts YAML.dump tree
        end
      end
    end
  end
end
