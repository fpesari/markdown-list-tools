# frozen_string_literal: true

# SPDX-License-Identifier: AGPL-3.0-or-later

module MarkdownListTools
  module Converter
    # MindMap (Freemind/Freeplane/XMind) converter
    module ToMindMap
      # Exception raised if a tree element is something other than a String or Hash
      class MalformedTreeError < RuntimeError
        def initialize(msg = 'Malformed tree')
          super msg
        end
      end

      class << self
        def convert(tree)
          puts '<map version="1.0.1">'
          puts "    <node TEXT=\"#{tree['title']}\">"
          build_node tree['list']
          puts '    </node>'
          puts '</map>'
        end

        private

        def build_node(list, level = 8)
          list.reduce([]) do |a, e|
            indent = ' ' * level
            a << case e
                 when String
                   puts "#{indent}<node TEXT=\"#{e}\"/>"
                 when Hash
                   puts "#{indent}<node TEXT=\"#{e.keys.first}\">"
                   build_node e.values.first, level + 4
                   puts "#{indent}</node>"
                 else
                   raise MalformedTreeError
                 end
            a
          end
        end
      end
    end
  end
end
