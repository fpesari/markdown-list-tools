# frozen_string_literal: true

# SPDX-License-Identifier: AGPL-3.0-or-later

require_relative 'markdown_list_tools/ast_parser'
require_relative 'markdown_list_tools/markdown_loader'
require_relative 'markdown_list_tools/converter/to_mind_map'
require_relative 'markdown_list_tools/converter/to_yaml'

# Main module
module MarkdownListTools
  # Executable programs
  class Program
    def initialize
      @banner = <<~USAGE
        #{@program_name} - Convert Markdown lists to #{@output_format} files
        Released under the GNU Affero General Public License 3, or any later version
        (C) 2020 Fabio Pesari

        Usage:

        #{@program_name} [FILE]

        Prints #{@output_format} output to the standard output.

        Example Markdown file:

        # Programming languages (this header is required and is the map center)

        * C
          * C++
            * D
            * Java
          * Objective C
          * Go
        * LISP
          * Scheme
            * Racket
          * Common Lisp
            * Clojure
        * Smalltalk
          * Ruby
            * Crystal
      USAGE
    end

    def quit(message)
      warn message
      exit(-1)
    end

    def run(arguments)
      quit(@banner) if arguments.length != 1
      ast = MarkdownLoader.load_file ARGV.last
      tree = ASTParser.parse ast[:children]
      @converter.convert tree
    end
  end

  # md2yaml
  class ToYAML < Program
    def initialize
      @program_name = 'list-md2yaml'
      @output_format = 'YAML'
      @converter = Converter::ToYAML
      super
    end
  end

  # md2mm
  class ToMindMap < Program
    def initialize
      @program_name = 'list-md2mm'
      @output_format = 'MindMap'
      @converter = Converter::ToMindMap
      super
    end
  end
end
